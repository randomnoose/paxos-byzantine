package application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.iosr.paxos.byzantine.io.Configuration;
import pl.edu.agh.iosr.paxos.byzantine.io.TransportLayer;
import pl.edu.agh.iosr.paxos.byzantine.messages.Message;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by patryklawski on 11/23/17.
 */
@Service
public class NetworkTransportLayer implements TransportLayer {

  @Autowired
  private ServerConfiguration serverConfiguration;

  private RestTemplate restTemplate = new RestTemplate();

  @Override
  public Configuration getConfiguration() {
    return Configuration.of(
        serverConfiguration.getAllNodesCount(),
        serverConfiguration.getFaultNodesLimit(),
        serverConfiguration.getTimeoutInMilliseconds()
    );
  }

  @Override
  public Set<Long> getPidsOfNodes() {
    return serverConfiguration.getNodes()
        .stream()
        .map(ServerConfiguration.Node::getId)
        .collect(Collectors.toSet());
  }

  @Override
  public void sendTo(Long pid, Message message) {
    String address = serverConfiguration.getNode(pid).getAddress();
    String url = "http://".concat(address).concat("/message");
    restTemplate.postForLocation(url, message);
  }

}
