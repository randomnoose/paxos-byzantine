package application;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import pl.edu.agh.iosr.paxos.byzantine.BasicNode;
import pl.edu.agh.iosr.paxos.byzantine.messages.Message;
import pl.edu.agh.iosr.paxos.byzantine.messages.MessageType;
import pl.edu.agh.iosr.paxos.byzantine.messages.StringProposal;

import java.util.UUID;


/**
 * Created by patryklawski on 11/22/17.
 */
@RestController
public class NodeController {

  private static final Logger LOGGER = LogManager.getLogger(NodeController.class);

//    @Autowired
//    private ApplicationContext appContext;
//
//  @Autowired
//  private ServerConfiguration serverConfiguration;
//
//  @RequestMapping(value = "/server-configuration", method = RequestMethod.GET)
//  public String getServerConfiguration() {
//    return serverConfiguration.toString();
//  }

//
//    @Autowired
//    private NodeEnv nodeEnv;    // wszystkie requesty POST (cały jeden :D) tu delegujemy

  @Autowired
  private BasicNode basicNode;

  @Autowired
  private TaskExecutor taskExecutor;

  @Autowired
  private ServerConfiguration serverConfiguration;

  private RestTemplate restTemplate = new RestTemplate();

  // TODO return node real decision
  @RequestMapping(value = "/get-decision", method = RequestMethod.GET)
  public String getDecision() {
    return "siedem";
  }


  @RequestMapping(value = "/start-session", method = RequestMethod.POST)
  public String startSession(@RequestBody String value) {
    UUID uuid = UUID.randomUUID();

    Message message = Message.builder()
        .pid(serverConfiguration.getOwnid())
        .type(MessageType.START_SESSION)
        .proposal(new StringProposal(value))
        .sessionId(uuid).build();

    taskExecutor.execute(() -> basicNode.notify(message));

    return "success: " + uuid;
  }

  @RequestMapping(value = "/message", method = RequestMethod.POST)
  public String message(@RequestBody Message message) {
    taskExecutor.execute(() -> basicNode.notify(message));
    return "Node queued to notify";
  }
}
