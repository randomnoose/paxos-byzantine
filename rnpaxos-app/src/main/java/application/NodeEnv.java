package application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import pl.edu.agh.iosr.paxos.byzantine.BasicNode;
import pl.edu.agh.iosr.paxos.byzantine.io.TransportLayer;

/**
 * Created by patryklawski on 11/23/17.
 */

@Component
public class NodeEnv {

  @Autowired
  private ServerConfiguration serverConfiguration;

  @Bean
  public BasicNode basicNode(TransportLayer networkTransportLayer) {
    BasicNode basicNode = new BasicNode(serverConfiguration.getOwnid());
    basicNode.configure(networkTransportLayer);
    return basicNode;
  }
}
