package application;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by patryklawski on 11/22/17.
 */
@Component
@ConfigurationProperties("server")
public class ServerConfiguration {

  private Long ownid;
  private List<Node> nodes = new ArrayList<>();
  private Long faultNodesLimit;
  private Long allNodesCount;
  private Long timeoutInMilliseconds;

  public Long getTimeoutInMilliseconds() { return timeoutInMilliseconds; }

  public void setTimeoutInMilliseconds(Long timeoutInMilliseconds) { this.timeoutInMilliseconds = timeoutInMilliseconds; }

  public Long getFaultNodesLimit() {
    return faultNodesLimit;
  }

  public void setFaultNodesLimit(Long faultNodesLimit) {
    this.faultNodesLimit = faultNodesLimit;
  }

  public Long getAllNodesCount() {
    return allNodesCount;
  }

  public void setAllNodesCount(Long allNodesCount) {
    this.allNodesCount = allNodesCount;
  }

  public Long getOwnid() {
    return ownid;
  }

  public void setOwnid(Long ownid) {
    this.ownid = ownid;
  }

  public List<Node> getNodes() {
    return nodes;
  }

  public void setNodes(List<Node> nodes) {
    this.nodes = nodes;
  }

  public Node getNode(Long id) {
    return nodes.stream().filter(node -> node.getId() == id).findFirst().orElse(null);
  }


  @Override
  public String toString() {
    return "{" +
        " faultNodesLimit = " + faultNodesLimit + "; " +
        " allNodesCount = " + allNodesCount + "; " +
        "ownid=\"" + ownid + '\"' +
        ", nodes=" + nodes +
        '}';
  }

  public static class Node {
    private Long id;
    private String address;

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public String getAddress() {
      return address;
    }

    public void setAddress(String address) {
      this.address = address;
    }

    @Override
    public String toString() {
      return "{" +
          "id=\"" + id + '\"' +
          ", address=\"" + address + '\"' +
          '}';
    }
  }

}
