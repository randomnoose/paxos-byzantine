package pl.edu.agh.iosr.paxos.byzantine;

import pl.edu.agh.iosr.paxos.byzantine.io.TransportLayer;
import pl.edu.agh.iosr.paxos.byzantine.messages.Message;
import pl.edu.agh.iosr.paxos.byzantine.messages.MessageType;
import pl.edu.agh.iosr.paxos.byzantine.messages.Proposal;
import pl.edu.agh.iosr.paxos.byzantine.node.acceptor.Acceptor;
import pl.edu.agh.iosr.paxos.byzantine.node.acceptor.AcceptorNode;
import pl.edu.agh.iosr.paxos.byzantine.node.acceptor.FaultyAcceptorNode;
import pl.edu.agh.iosr.paxos.byzantine.node.proposer.Proposer;
import pl.edu.agh.iosr.paxos.byzantine.node.proposer.ProposerNode;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.UUID;

public class BasicNode implements Acceptor, Proposer {

  private final long pid;
  private Acceptor acceptor;
  private Proposer proposer;

  public BasicNode(long pid) {
    this.pid = pid;
    acceptor = new AcceptorNode(pid);
    proposer = new ProposerNode(pid);
  }

  private BasicNode(long pid, Acceptor acceptor, Proposer proposer) {
    this.pid = pid;
    this.acceptor = acceptor;
    this.proposer = proposer;
  }

  public void configure(TransportLayer transportLayer) {
    acceptor.configure(transportLayer);
    proposer.configure(transportLayer);
  }

  public long getPid() {
    return pid;
  }

  @Override
  public Proposal getDecision(UUID sessionId) {
    return acceptor.getDecision(sessionId);
  }

  @Override
  public MessageType getBaseOfDecision(UUID sessionId) {
    return acceptor.getBaseOfDecision(sessionId);
  }

  @Override
  public void notify(Message message) {
    switch (message.getType()) {
      case NOTIFY_PROPOSAL:
      case NOTIFY_WEAK:
      case NOTIFY_STRONG:
      case NOTIFY_DECISION:
      case NOTIFY_FREEZE:
      case INTERNAL_ROUND_TIMEOUT:
        acceptor.notify(message);
        break;
      case START_SESSION:
      case SIGNED_PROOF_TO_PROPOSER:
        proposer.notify(message);
        break;
      default:
        break;
    }
  }

  public static BasicNode createWithFaultyAcceptor(long pid) {
    return new BasicNode(pid, new FaultyAcceptorNode(pid, 1), new ProposerNode(pid));
  }

  public static BasicNode create() {
    throw new NotImplementedException();
  }
}
