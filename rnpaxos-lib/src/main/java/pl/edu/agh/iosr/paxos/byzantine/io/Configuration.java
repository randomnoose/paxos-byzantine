package pl.edu.agh.iosr.paxos.byzantine.io;

import lombok.Getter;
import lombok.ToString;

@ToString
public final class Configuration {

  @Getter
  private final long allNodesCount;
  @Getter
  private final long faultNodesLimit;
  @Getter
  private final long timeoutInMilliseconds;

  public Configuration(long allNodesCount, long faultNodesLimit, long timeoutInMilliseconds) {
    this.allNodesCount = allNodesCount;
    this.faultNodesLimit = faultNodesLimit;
    this.timeoutInMilliseconds = timeoutInMilliseconds;
  }

  public static Configuration of(long nodesCount, long faultyLimit, long roundTimeoutMilliseconds) {
    return new Configuration(nodesCount, faultyLimit, roundTimeoutMilliseconds);
  }
}
