package pl.edu.agh.iosr.paxos.byzantine.io;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.iosr.paxos.byzantine.messages.Message;

public class NetworkManager {

  private static final Logger LOGGER = LogManager.getLogger(NetworkManager.class);

  private final TransportLayer transportLayer;
  private Configuration configuration;

  public NetworkManager(TransportLayer transportLayer) {
    this.transportLayer = transportLayer;
  }

  public void init() {
    configuration = transportLayer.getConfiguration();
  }

  public long getAllNodes() {
    return configuration.getAllNodesCount();
  }

  public long getFaultyNodesLimit() {
    return configuration.getFaultNodesLimit();
  }

  public long getTimeoutInMilliseconds() {
    return configuration.getTimeoutInMilliseconds();
  }

  public void notifyAll(Message message) {
    transportLayer.getPidsOfNodes().forEach(pid -> {
      transportLayer.sendTo(pid, message);
      logTraffic(pid, message);
    });
  }

  public void notifyOthers(Message message) {
    transportLayer.getPidsOfNodes().stream()
        .filter(pid -> pid != message.getPid())
        .forEach(pid -> {
          transportLayer.sendTo(pid, message);
          logTraffic(pid, message);
        });
  }

  public void notifyMe(Message message) {
    long myPid = message.getPid();
    transportLayer.sendTo(myPid, message);
    logTraffic(myPid, message);
  }

  private static void logTraffic(long otherPid, Message message) {
    LOGGER.trace(String.format("[%d] -> [%d]: %s", message.getPid(), otherPid, message));
  }
}
