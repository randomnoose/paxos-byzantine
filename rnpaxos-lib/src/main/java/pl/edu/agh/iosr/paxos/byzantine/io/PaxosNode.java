package pl.edu.agh.iosr.paxos.byzantine.io;

import pl.edu.agh.iosr.paxos.byzantine.messages.Message;

public interface PaxosNode {

  void notify(Message message);

  void configure(TransportLayer transportLayer);
}
