package pl.edu.agh.iosr.paxos.byzantine.io;

import pl.edu.agh.iosr.paxos.byzantine.messages.Message;

import java.util.Set;

public interface TransportLayer {

  Configuration getConfiguration();

  Set<Long> getPidsOfNodes();

  void sendTo(Long pid, Message message);
}
