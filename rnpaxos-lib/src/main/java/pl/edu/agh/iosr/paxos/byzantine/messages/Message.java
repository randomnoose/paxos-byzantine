package pl.edu.agh.iosr.paxos.byzantine.messages;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;
import java.util.UUID;

@Builder
@ToString
public class Message {

  @Getter
  private MessageType type;
  @Getter
  private UUID sessionId;
  @Getter
  private long pid;
  @Getter
  private long roundNumber;
  @Getter
  private Proposal proposal;
  @Getter
  private List<Proof> proofs;
}

