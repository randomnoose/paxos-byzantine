package pl.edu.agh.iosr.paxos.byzantine.messages;

import java.util.List;
import java.util.UUID;

public class MessageFactory {

  public static Message createProposal(UUID sessionId, Proposal proposal, long pid) {
    return prepareNotificationBase(sessionId, -1, proposal, pid)
        .type(MessageType.NOTIFY_PROPOSAL)
        .build();
  }

  //TODO don't take pid from there
  public static Message createWeakNotification(UUID sessionId, long roundNumber, Proposal proposal, long pid) {
    return prepareNotificationBase(sessionId, roundNumber, proposal, pid)
        .type(MessageType.NOTIFY_WEAK)
        .build();
  }

  public static Message createStrongNotification(UUID sessionId, long roundNumber, Proposal proposal, long pid) {
    return prepareNotificationBase(sessionId, roundNumber, proposal, pid)
        .type(MessageType.NOTIFY_STRONG)
        .build();
  }

  public static Message createDecisionNotification(UUID sessionId, long roundNumber, Proposal proposal, long pid) {
    return prepareNotificationBase(sessionId, roundNumber, proposal, pid)
        .type(MessageType.NOTIFY_DECISION)
        .build();
  }

  public static Message createFreezeNotification(UUID sessionId, long roundNumber, long pid) {
    return prepareNotificationBase(sessionId, roundNumber, null, pid)
        .type(MessageType.NOTIFY_FREEZE)
        .build();
  }

  public static Message createTimeout(UUID sessionId, long roundNumber, long pid) {
    return prepareNotificationBase(sessionId, roundNumber, null, pid)
        .type(MessageType.INTERNAL_ROUND_TIMEOUT)
        .build();
  }

  public static Message createProofsNotification(UUID sessionId, long roundNumber, long pid, List<Proof> signedProofs) {
    return prepareNotificationBase(sessionId, roundNumber, null, pid)
        .type(MessageType.SIGNED_PROOF_TO_PROPOSER)
        .proofs(signedProofs)
        .build();
  }

  private static Message.MessageBuilder prepareNotificationBase(UUID sessionId, long roundNumber, Proposal proposal, long pid) {
    return Message.builder()
        .pid(pid)
        .sessionId(sessionId)
        .roundNumber(roundNumber)
        .proposal(proposal);
  }
}
