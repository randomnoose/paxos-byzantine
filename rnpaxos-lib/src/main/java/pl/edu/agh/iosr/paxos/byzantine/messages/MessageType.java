package pl.edu.agh.iosr.paxos.byzantine.messages;

import lombok.Getter;

public enum MessageType {
  NOTIFY_PROPOSAL("000"),
  NOTIFY_WEAK("001"),
  NOTIFY_STRONG("002"),
  NOTIFY_DECISION("003"),
  NOTIFY_FREEZE("004"),
  SIGNED_PROOF_TO_ACCEPTOR("005"),
  INTERNAL_ROUND_TIMEOUT("010"),

  START_SESSION("100"),
  SIGNED_PROOF_TO_PROPOSER("105");

  @Getter
  private final String code;

  MessageType(String code) {
    this.code = code;
  }
}
