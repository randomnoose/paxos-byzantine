package pl.edu.agh.iosr.paxos.byzantine.messages;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.UUID;

@Builder
@ToString
public class Proof {

  @Getter
  private final UUID sessionId;
  @Getter
  private final long roundId;
  @Getter
  private final long pid;
  @Getter
  private final Proposal weak;
  @Getter
  private final Proposal strong;
}
