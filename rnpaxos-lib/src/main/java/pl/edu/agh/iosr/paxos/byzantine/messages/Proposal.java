package pl.edu.agh.iosr.paxos.byzantine.messages;

public abstract class Proposal {

  public static final Proposal FROZEN_PROPOSAL = new Proposal() {
    @Override
    public int hashCode() {
      return 0;
    }

    @Override
    public boolean equals(Object obj) {
      return FROZEN_PROPOSAL == obj;
    }
  };

  public abstract int hashCode();

  public abstract boolean equals(Object obj);
}
