package pl.edu.agh.iosr.paxos.byzantine.messages;

import lombok.Getter;
import lombok.ToString;

@ToString
public final class StringProposal extends Proposal {

  @Getter
  private final String value;

  public StringProposal(String value) {
    this.value = value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    StringProposal that = (StringProposal) o;

    return value.equals(that.value);
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }
}
