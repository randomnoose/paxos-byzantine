package pl.edu.agh.iosr.paxos.byzantine.node.acceptor;

import pl.edu.agh.iosr.paxos.byzantine.io.PaxosNode;
import pl.edu.agh.iosr.paxos.byzantine.messages.MessageType;
import pl.edu.agh.iosr.paxos.byzantine.messages.Proposal;

import java.util.UUID;

public interface Acceptor extends PaxosNode {

  Proposal getDecision(UUID sessionId);

  MessageType getBaseOfDecision(UUID sessionId);
}
