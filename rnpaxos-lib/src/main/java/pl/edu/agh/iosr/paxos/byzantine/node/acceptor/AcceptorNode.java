package pl.edu.agh.iosr.paxos.byzantine.node.acceptor;

import javafx.util.Callback;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.iosr.paxos.byzantine.io.NetworkManager;
import pl.edu.agh.iosr.paxos.byzantine.io.TransportLayer;
import pl.edu.agh.iosr.paxos.byzantine.messages.*;
import pl.edu.agh.iosr.paxos.byzantine.node.session.AcceptorRound;
import pl.edu.agh.iosr.paxos.byzantine.node.session.AcceptorSession;
import pl.edu.agh.iosr.paxos.byzantine.node.session.RoundHelper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class AcceptorNode implements Acceptor {

  private static final Logger LOGGER = LogManager.getLogger(AcceptorNode.class);

  private final ReentrantLock lock = new ReentrantLock();
  private Map<UUID, AcceptorSession> sessions = new HashMap<>();
  private RoundHelper roundHelper = new RoundHelper();
  private boolean isReady;

  protected final long pid;
  protected NetworkManager networkManager;

  public AcceptorNode(long pid) {
    this.pid = pid;
  }

  @Override
  public void configure(TransportLayer transportLayer) {
    networkManager = new NetworkManager(transportLayer);
    networkManager.init();
    roundHelper.setNetworkManager(networkManager);
    isReady = true;
  }

  @Override
  public Proposal getDecision(UUID sessionId) {
    return Optional.ofNullable(sessions.get(sessionId))
        .map(AcceptorSession::getLastRound)
        .map(AcceptorRound::getDecision)
        .orElse(null);
  }

  @Override
  public MessageType getBaseOfDecision(UUID sessionId) {
    return Optional.ofNullable(sessions.get(sessionId))
        .map(AcceptorSession::getLastRound)
        .map(AcceptorRound::getBaseOfDecision)
        .orElse(null);
  }

  @Override
  public void notify(Message message) {
    if (!isReady) {
      throw new IllegalStateException();
    }

    lock.lock();
    LOGGER.info(String.format("Receiving: %s", message));
    switch (message.getType()) {
      case NOTIFY_PROPOSAL:
        handlePropose(message);
        break;
      case NOTIFY_WEAK:
        handleWeakNotification(message);
        break;
      case NOTIFY_STRONG:
        handleStrongNotification(message);
        break;
      case NOTIFY_DECISION:
        handleDecisionNotification(message);
        break;
      case NOTIFY_FREEZE:
        handleFreezeNotification(message);
        break;
      case INTERNAL_ROUND_TIMEOUT:
        handleTimeout(message);
        break;
      case SIGNED_PROOF_TO_ACCEPTOR:
        handleSignedProof(message);
        break;
      default:
        // out of context
    }
    lock.unlock();
  }

  protected void handlePropose(Message message) {
    AcceptorSession session = getSession(message.getSessionId());
    session.getInitialRound(networkManager.getTimeoutInMilliseconds(), getTimeoutCallback(message.getSessionId()));
    // TODO: retrieve out-of-context messages
    if (isGood(message.getProposal())) {
      Message notification = MessageFactory.createWeakNotification(session.getId(), AcceptorSession.INITIAL_ROUND_NUMBER, message.getProposal(), pid);
      networkManager.notifyOthers(notification);
      networkManager.notifyMe(notification);
    } else {
      LOGGER.info("Proposed value is not GOOD");
    }
  }

  protected void handleWeakNotification(Message message) {
    AcceptorSession session = getSession(message.getSessionId());
    AcceptorRound round = session.getRound(message.getRoundNumber());

    round.addWeakNotification(message.getPid(), message.getProposal());
    if (roundHelper.canStronglyAgreeBasedOnWeak(round, message)) {
      round.setStrongDecision(message.getProposal());
      Message strongNotification = MessageFactory.createStrongNotification(session.getId(), round.getNumber(), message.getProposal(), pid);
      networkManager.notifyOthers(strongNotification);
      networkManager.notifyMe(strongNotification);
    }

    if (roundHelper.canDecideBasedOnWeak(round, message)) {
      round.setDecision(message.getProposal(), MessageType.NOTIFY_WEAK);
      Message decisionNotification = MessageFactory.createDecisionNotification(session.getId(), round.getNumber(), message.getProposal(), pid);
      networkManager.notifyOthers(decisionNotification);
      networkManager.notifyMe(decisionNotification);
    }
  }

  protected void handleStrongNotification(Message message) {
    AcceptorSession session = getSession(message.getSessionId());
    AcceptorRound round = session.getRound(message.getRoundNumber());

    round.addStrongNotification(message.getPid(), message.getProposal());
    if (roundHelper.canDecideBasedOnStrong(round, message)) {
      round.setDecision(message.getProposal(), MessageType.NOTIFY_STRONG);
      Message decisionNotification = MessageFactory.createDecisionNotification(session.getId(), round.getNumber(), message.getProposal(), pid);
      networkManager.notifyOthers(decisionNotification);
      networkManager.notifyMe(decisionNotification);
    }
  }

  protected void handleDecisionNotification(Message message) {
    AcceptorSession session = getSession(message.getSessionId());
    AcceptorRound round = session.getRound(message.getRoundNumber());

    round.addDecisionNotification(message.getPid(), message.getProposal());
    if (roundHelper.canDecideBasedOnDecided(round, message)) {
      round.setDecision(message.getProposal(), MessageType.NOTIFY_DECISION);
      Message decisionNotification = MessageFactory.createDecisionNotification(session.getId(), round.getNumber(), message.getProposal(), pid);
      networkManager.notifyOthers(decisionNotification);
      networkManager.notifyMe(decisionNotification);
    }
  }

  protected void handleTimeout(Message message) {
    AcceptorSession session = getSession(message.getSessionId());
    AcceptorRound round = session.getRound(message.getRoundNumber());

    round.setWeakDecision(Proposal.FROZEN_PROPOSAL);
    round.setStrongDecision(Proposal.FROZEN_PROPOSAL);
    Message freezeNotification = MessageFactory.createFreezeNotification(session.getId(), round.getNumber(), pid);
    networkManager.notifyOthers(freezeNotification);
    networkManager.notifyMe(freezeNotification);
  }

  protected void handleFreezeNotification(Message message) {
    AcceptorSession session = getSession(message.getSessionId());
    AcceptorRound round = session.getRound(message.getRoundNumber());

    round.addFreezeNotification(message.getPid());
    if (roundHelper.canWeaklyFreeze(round)) {
      round.setFrozen();
      round.setWeakDecision(Proposal.FROZEN_PROPOSAL);
      round.setStrongDecision(Proposal.FROZEN_PROPOSAL);
      Message freezeNotification = MessageFactory.createFreezeNotification(session.getId(), round.getNumber(), pid);
      networkManager.notifyOthers(freezeNotification);
      networkManager.notifyMe(freezeNotification);

      List<Proof> signedProofs = getSignedProofs(session.getId(), round.getNumber() + 1);
      Message proofsNotification = MessageFactory.createProofsNotification(session.getId(), round.getNumber(), pid, signedProofs);
      networkManager.notifyAll(proofsNotification);
    }

    if (roundHelper.canStronglyFreeze(round)) {
      round.setStronglyFrozen();
    }

    if (!round.isNextStarted() && roundHelper.allPreviousRoundsFrozen(session)) {
      round.setNextStarted();
      session.getNextRoundWithTimeout(networkManager.getTimeoutInMilliseconds(), getTimeoutCallback(session.getId()));
    }

  }

  protected void handleSignedProof(Message message) {
    throw new NotImplementedException();
  }

  protected boolean isGood(Proposal proposal) {
    return true;
  }

  protected AcceptorSession getSession(UUID sessionId) {
    if (sessions.containsKey(sessionId)) {
      return sessions.get(sessionId);
    }

    AcceptorSession session = new AcceptorSession(sessionId);
    sessions.put(sessionId, session);
    return session;
  }

  protected Callback<Long, Void> getTimeoutCallback(UUID sessionId) {
    return roundNumber -> {
      networkManager.notifyMe(MessageFactory.createTimeout(sessionId, roundNumber, pid));
      return null;
    };
  }

  protected List<Proof> getSignedProofs(UUID sessionId, long roundNumber) {
    return sessions.get(sessionId).getRounds().entrySet().stream()
        .filter(entry -> entry.getValue().getNumber() < roundNumber)
        .map(entry -> getProof(sessionId, entry.getValue()))
        .collect(Collectors.toList());
  }

  protected Proof getProof(UUID sessionId, AcceptorRound round) {
    return Proof.builder()
        .sessionId(sessionId)
        .roundId(round.getNumber())
        .pid(pid)
        .weak(round.getWeakDecision())
        .strong(round.getStrongDecision())
        .build();
  }
}
