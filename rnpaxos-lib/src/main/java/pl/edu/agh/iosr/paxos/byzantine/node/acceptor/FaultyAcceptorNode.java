package pl.edu.agh.iosr.paxos.byzantine.node.acceptor;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.iosr.paxos.byzantine.messages.Message;
import pl.edu.agh.iosr.paxos.byzantine.messages.MessageFactory;
import pl.edu.agh.iosr.paxos.byzantine.messages.StringProposal;
import pl.edu.agh.iosr.paxos.byzantine.node.session.AcceptorSession;

import java.util.UUID;

public class FaultyAcceptorNode extends AcceptorNode {

  private static final Logger LOGGER = LogManager.getLogger(FaultyAcceptorNode.class);
  private int faultMessagesCount;

  public FaultyAcceptorNode(long pid, int faultMessagesCount) {
    super(pid);
    this.faultMessagesCount = faultMessagesCount;
  }

  @Override
  protected void handlePropose(Message message) {
    AcceptorSession session = getSession(message.getSessionId());
    session.getInitialRound(networkManager.getTimeoutInMilliseconds(), getTimeoutCallback(message.getSessionId()));
    // TODO: retrieve out-of-context messages
    if (isGood(message.getProposal())) {
      Message notification = MessageFactory.createWeakNotification(session.getId(), AcceptorSession.INITIAL_ROUND_NUMBER, message.getProposal(), pid);
      if (faultMessagesCount > 0) {
        faultMessagesCount--;
        notification = createFaultyMessage(session.getId(), AcceptorSession.INITIAL_ROUND_NUMBER, pid);
      }
      networkManager.notifyOthers(notification);
      networkManager.notifyMe(notification);
    } else {
      LOGGER.info("Proposed value is not GOOD");
    }
  }

  private Message createFaultyMessage(UUID sessionId, long roundNumber, long pid) {
    return MessageFactory.createWeakNotification(sessionId, roundNumber, new StringProposal("FAULTY_PROPOSAL"), pid);
  }
}
