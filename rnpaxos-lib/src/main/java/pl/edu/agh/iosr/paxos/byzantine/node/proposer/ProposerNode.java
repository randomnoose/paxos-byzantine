package pl.edu.agh.iosr.paxos.byzantine.node.proposer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.edu.agh.iosr.paxos.byzantine.io.NetworkManager;
import pl.edu.agh.iosr.paxos.byzantine.io.TransportLayer;
import pl.edu.agh.iosr.paxos.byzantine.messages.Message;
import pl.edu.agh.iosr.paxos.byzantine.messages.MessageFactory;
import pl.edu.agh.iosr.paxos.byzantine.messages.Proof;
import pl.edu.agh.iosr.paxos.byzantine.messages.Proposal;
import pl.edu.agh.iosr.paxos.byzantine.node.session.ProposerRound;
import pl.edu.agh.iosr.paxos.byzantine.node.session.ProposerSession;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

public class ProposerNode implements Proposer {

  private static final Logger LOGGER = LogManager.getLogger(ProposerNode.class);

  private final ReentrantLock lock = new ReentrantLock();
  private final long pid;

  private Map<UUID, ProposerSession> sessions = new HashMap<>();
  private NetworkManager networkManager;
  private boolean isReady;

  public ProposerNode(long pid) {
    this.pid = pid;
  }

  @Override
  public void configure(TransportLayer transportLayer) {
    networkManager = new NetworkManager(transportLayer);
    networkManager.init();
    isReady = true;
  }

  @Override
  public void notify(Message message) {
    if (!isReady) {
      throw new IllegalStateException("Proposer node is not configured");
    }

    lock.lock();
    LOGGER.trace(message);
    switch (message.getType()) {
      case START_SESSION:
        handleStartSession(message);
        break;
      case SIGNED_PROOF_TO_PROPOSER:
        handleSignedProof(message);
        break;
      default:
        handleOutOfContext(message);
    }
    lock.unlock();
  }

  private void handleStartSession(Message message) {
    UUID sessionId = Optional.ofNullable(message.getSessionId()).orElseGet(UUID::randomUUID);
    if (isGood(message.getProposal())) {
      networkManager.notifyAll(MessageFactory.createProposal(sessionId, message.getProposal(), pid));
    } else {
      LOGGER.info("Proposed value %s is not GOOD", message.getProposal());
    }
  }

  private boolean isGood(Proposal proposal) {
//    List<Proposal> acc = getAcc(null);
//    Set<Proposal> poss = getPoss(null);
//    if (poss.isEmpty())

    return true;
  }

  private void handleSignedProof(Message message) {
//    veryfiSignature(message) // remove bullshit
    ProposerSession session = getSession(message.getSessionId());
    ProposerRound round = session.getRoundOrCreate(message.getRoundNumber());
    message.getProofs().forEach(round::addProof);
  }

  private void handleOutOfContext(Message message) {
    throw new NotImplementedException();
  }

  private ProposerSession getSession(UUID sessionId) {
    if (sessions.containsKey(sessionId)) {
      return sessions.get(sessionId);
    }

    ProposerSession session = new ProposerSession(sessionId);
    sessions.put(sessionId, session);
    return session;
  }

  private List<Proposal> getAcc(List<Proof> proofs) {
    List<Proposal> acceptedProposals = new ArrayList<>();
    Map<Proposal, Long> weakByCount = proofs.stream()
        .filter(proof -> proof.getWeak() != Proposal.FROZEN_PROPOSAL)
        .map(Proof::getWeak)
        .collect(Collectors.groupingBy(weak -> weak, Collectors.counting()));

    weakByCount.forEach((proposal, count) -> {
      if (count > networkManager.getFaultyNodesLimit()) {
        acceptedProposals.add(proposal);
      }
    });

    return acceptedProposals;
  }

  private Set<Proposal> getPoss(List<Proof> proofs) {
    Set<Proposal> possibleProposals = new HashSet<>();

    Map<Proposal, Long> weakByCount = proofs.stream()
        .filter(proof -> proof.getWeak() != Proposal.FROZEN_PROPOSAL)
        .map(Proof::getWeak)
        .collect(Collectors.groupingBy(weak -> weak, Collectors.counting()));

    weakByCount.forEach((proposal, count) -> {
      if (count > (networkManager.getAllNodes() + networkManager.getFaultyNodesLimit()) / 2) {
        possibleProposals.add(proposal);
      }
    });

    Map<Proposal, Long> strongByCount = proofs.stream()
        .filter(proof -> proof.getStrong() != Proposal.FROZEN_PROPOSAL)
        .map(Proof::getStrong)
        .collect(Collectors.groupingBy(strong -> strong, Collectors.counting()));
    strongByCount.forEach((proposal, count) -> {
      if (count > networkManager.getFaultyNodesLimit()) {
        possibleProposals.add(proposal);
      }
    });

    return possibleProposals;
  }
}
