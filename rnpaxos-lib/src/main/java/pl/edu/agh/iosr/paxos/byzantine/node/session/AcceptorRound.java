package pl.edu.agh.iosr.paxos.byzantine.node.session;

import javafx.util.Callback;
import lombok.Getter;
import lombok.ToString;
import pl.edu.agh.iosr.paxos.byzantine.messages.MessageType;
import pl.edu.agh.iosr.paxos.byzantine.messages.Proposal;

import java.util.*;

@ToString
public class AcceptorRound {

  @Getter
  private long number;
  @Getter
  private Proposal weakDecision;
  @Getter
  private Proposal strongDecision;
  @Getter
  private Proposal decision;
  @Getter
  private MessageType baseOfDecision;
  @Getter
  private boolean weaklyFrozen;
  @Getter
  private boolean stronglyFrozen;
  @Getter
  private boolean isNextStarted;
  private Timer timer;

  @Getter
  private Map<Long, Proposal> weakNotifications = new HashMap<>();
  @Getter
  private Map<Long, Proposal> strongNotifications = new HashMap<>();
  @Getter
  private Map<Long, Proposal> decisionNotifications = new HashMap<>();
  @Getter
  private Set<Long> freezeNotifications = new HashSet<>();

  AcceptorRound(long number) {
    this.number = number;
  }

  public void addWeakNotification(long pid, Proposal value) {
    weakNotifications.putIfAbsent(pid, value);
  }

  public void addStrongNotification(long pid, Proposal value) {
    strongNotifications.putIfAbsent(pid, value);
  }

  public void addDecisionNotification(long pid, Proposal value) {
    decisionNotifications.putIfAbsent(pid, value);
  }

  public void addFreezeNotification(long pid) {
    freezeNotifications.add(pid);
  }

  public void setWeakDecision(Proposal weakDecision) {
    if (this.weakDecision == null) {
      this.weakDecision = weakDecision;
    }
  }

  public void setStrongDecision(Proposal strongDecision) {
    if (this.strongDecision == null) {
      this.strongDecision = strongDecision;
    }
  }

  public void setDecision(Proposal decision, MessageType baseOfDecision) {
    if (this.decision == null) {
      this.decision = decision;
      this.baseOfDecision = baseOfDecision;
    }
  }

  public void setFrozen() {
    weaklyFrozen = true;
  }

  public void setStronglyFrozen() {
    stronglyFrozen = true;
  }

  public void setNextStarted() {
    isNextStarted = true;
  }

  static AcceptorRound createWithTimer(long roundNumber, Callback<Long, Void> callback, long timeoutMilliseconds) {
    AcceptorRound round = new AcceptorRound(roundNumber);
    round.setTimer(callback, roundNumber, timeoutMilliseconds);
    return round;
  }

  private void setTimer(Callback<Long, Void> callback, long roundNumber, long timeoutMilliseconds) {
    timer = new Timer();
    timer.schedule(new TimerTask() {
      @Override
      public void run() {
        if (decision == null) {
          callback.call(roundNumber);
        }
        timer.cancel();
        timer.purge();
      }
    }, timeoutMilliseconds);
  }
}
