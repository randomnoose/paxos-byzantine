package pl.edu.agh.iosr.paxos.byzantine.node.session;

import javafx.util.Callback;
import lombok.Getter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@ToString
public class AcceptorSession {

  public static final long INITIAL_ROUND_NUMBER = 1L;

  @Getter
  private final UUID id;
  @Getter
  private Map<Long, AcceptorRound> rounds = new HashMap<>();

  public AcceptorSession(UUID id) {
    this.id = id;
  }

  public AcceptorRound getRound(long roundNumber) {
    return rounds.get(roundNumber);
  }

  public AcceptorRound getLastRound() {
    long lastRound = rounds.keySet().stream().mapToLong(value -> value).max().orElse(0);
    if (lastRound == 0) {
      return null;
    }
    return rounds.get(lastRound);
  }

  public AcceptorRound getNextRoundWithTimeout(long timeoutMilliseconds, Callback<Long, Void> callback) {
    long nextRoundNumber = getLastRound().getNumber() + 1;
    AcceptorRound round = AcceptorRound.createWithTimer(nextRoundNumber, callback, timeoutMilliseconds);
    rounds.put(nextRoundNumber, round);
    return round;
  }

  public AcceptorRound getInitialRound(long timeoutMilliseconds, Callback<Long, Void> callback) {
    AcceptorRound round = AcceptorRound.createWithTimer(INITIAL_ROUND_NUMBER, callback, timeoutMilliseconds);
    rounds.put(INITIAL_ROUND_NUMBER, round);
    return round;
  }
}
