package pl.edu.agh.iosr.paxos.byzantine.node.session;

import pl.edu.agh.iosr.paxos.byzantine.messages.Proof;

import java.util.HashMap;
import java.util.Map;

public class ProposerRound {

  private final long number;
  private Map<Long, Proof> proofsPerRound = new HashMap<>();

  public ProposerRound(long number) {
    this.number = number;
  }

  public void addProof(Proof proof) {
    proofsPerRound.put(proof.getRoundId(), proof);
  }

}
