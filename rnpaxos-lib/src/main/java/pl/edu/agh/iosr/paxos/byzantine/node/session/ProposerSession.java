package pl.edu.agh.iosr.paxos.byzantine.node.session;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ProposerSession {

  @Getter
  private final UUID sessionId;
  @Getter
  private Map<Long, ProposerRound> rounds = new HashMap<>();

  public ProposerSession(UUID sessionId) {
    this.sessionId = sessionId;
  }

  public ProposerRound getRoundOrCreate(long roundNumber) {
    ProposerRound round = rounds.get(roundNumber);
    if (round == null) {
      round = new ProposerRound(roundNumber);
      rounds.put(roundNumber, round);
    }
    return round;
  }
}
