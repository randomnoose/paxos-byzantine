package pl.edu.agh.iosr.paxos.byzantine.node.session;

import lombok.Setter;
import pl.edu.agh.iosr.paxos.byzantine.io.NetworkManager;
import pl.edu.agh.iosr.paxos.byzantine.messages.Message;
import pl.edu.agh.iosr.paxos.byzantine.messages.Proposal;

import java.util.Set;

public class RoundHelper {

  @Setter
  private NetworkManager networkManager;

  public boolean canWeaklyAgree(AcceptorRound round, Message message) {
    return round.getWeakDecision() == null && isGood(message.getProposal());
  }

  public boolean canStronglyAgreeBasedOnWeak(AcceptorRound round, Message message) {
    if (round.getStrongDecision() != null) {
      return false;
    } else {
      long weakCount = round.getWeakNotifications().values().stream()
          .filter(decision -> decision.equals(message.getProposal()))
          .count();
      return weakCount > getWeakQuorum();
    }
  }

  public boolean canDecideBasedOnWeak(AcceptorRound round, Message message) {
    if (round.getDecision() != null) {
      return false;
    } else {
      long weakCount = round.getWeakNotifications().values().stream()
          .filter(decision -> decision.equals(message.getProposal()))
          .count();
      return weakCount > getWeakQuorumForDecision();
    }
  }

  public boolean canDecideBasedOnStrong(AcceptorRound round, Message message) {
    if (round.getDecision() != null) {
      return false;
    } else {
      long strongCount = round.getStrongNotifications().values().stream()
          .filter(decision -> decision.equals(message.getProposal()))
          .count();
      return strongCount > getStrongQuorum();
    }
  }

  public boolean canDecideBasedOnDecided(AcceptorRound round, Message message) {
    if (round.getDecision() != null) {
      return false;
    } else {
      long decidedCount = round.getDecisionNotifications().values().stream()
          .filter(decision -> decision.equals(message.getProposal()))
          .count();
      return decidedCount > getQuorum();
    }
  }

  private boolean isGood(Proposal proposal) {
    return true;
  }

  private long getWeakQuorum() {
    long allNodesCount = networkManager.getAllNodes();
    long faultyNodesLimit = networkManager.getFaultyNodesLimit();
    return (allNodesCount + faultyNodesLimit) / 2;
  }

  private long getWeakQuorumForDecision() {
    long allNodesCount = networkManager.getAllNodes();
    long faultyNodesLimit = networkManager.getFaultyNodesLimit();
    return (allNodesCount + 3 * faultyNodesLimit) / 2;
  }

  private long getStrongQuorum() {
    long faultyNodesLimit = networkManager.getFaultyNodesLimit();
    return 2 * faultyNodesLimit;
  }

  private long getQuorum() {
    return networkManager.getFaultyNodesLimit();
  }

  public boolean canWeaklyFreeze(AcceptorRound round) {
    if (round.isWeaklyFrozen()) {
      return false;
    } else {
      long faultyNodesLimit = networkManager.getFaultyNodesLimit();
      return round.getFreezeNotifications().size() > faultyNodesLimit;
    }
  }

  public boolean canStronglyFreeze(AcceptorRound round) {
    if (round.isStronglyFrozen()) {
      return false;
    } else {
      long faultyNodesLimit = networkManager.getFaultyNodesLimit();
      return round.getFreezeNotifications().size() > 2 * faultyNodesLimit;
    }
  }

  public boolean allPreviousRoundsFrozen(AcceptorSession session) {
    Set<Long> roundsNumbers = session.getRounds().keySet();
    for (long roundNumber : roundsNumbers) {
      AcceptorRound round = session.getRound(roundNumber);
      if (!round.isStronglyFrozen()) {
        return false;
      }
    }
    return true;
  }
}
