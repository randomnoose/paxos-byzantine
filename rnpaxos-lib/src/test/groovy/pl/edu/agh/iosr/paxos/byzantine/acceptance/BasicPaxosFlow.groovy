package pl.edu.agh.iosr.paxos.byzantine.acceptance

import pl.edu.agh.iosr.paxos.byzantine.BasicNode
import pl.edu.agh.iosr.paxos.byzantine.io.Configuration
import pl.edu.agh.iosr.paxos.byzantine.messages.Message
import pl.edu.agh.iosr.paxos.byzantine.messages.MessageType
import pl.edu.agh.iosr.paxos.byzantine.messages.Proposal

class BasicPaxosFlow {

    private List<Thread> nodes = new ArrayList<>()

    private List<NodeLocalTestEnvironment> distinctEnvironments = new ArrayList<>()
    private List<NodeLocalTestEnvironment> acceptorEnvironments = new ArrayList<>()
    private List<NodeLocalTestEnvironment> proposersEnvironments = new ArrayList<>()
    private NodeLocalTestEnvironment leaderEnvironment

    private NodeLocalTestTransportLayer transportLayer
    private Configuration configuration
    private Proposal proposal
    private UUID firstSessionId

    private BasicPaxosFlow() {}

    static BasicPaxosFlow flow() {
        return new BasicPaxosFlow()
    }

    BasicPaxosFlow withNode(BasicNode node, List<Role> roles) {
        def environment = createEnvironment(node)
        distinctEnvironments.add(environment)
        if (roles.contains(Role.ACCEPTOR)) {
            acceptorEnvironments.add(environment)
        }
        if (roles.contains(Role.PROPOSER)) {
            proposersEnvironments.add(environment)
        }
        this
    }

    BasicPaxosFlow withNodes(Collection<BasicNode> nodes, List<Role> roles) {
        nodes.each { node ->
            withNode(node, roles)
        }
        this
    }

    BasicPaxosFlow withConfiguration(Configuration configuration) {
        this.configuration = configuration
        this
    }

    BasicPaxosFlow withProposal(Proposal proposal, firstSessionId) {
        this.proposal = proposal
        this.firstSessionId = firstSessionId
        this
    }

    BasicPaxosFlow withFirstLeader(BasicNode node) {
        leaderEnvironment = distinctEnvironments.find { it.node == node }
        this
    }

    void run() {
        verify()
        transportLayer = new NodeLocalTestTransportLayer(configuration)

        distinctEnvironments.each { environment ->
            environment.transportLayer = transportLayer
            environment.configure()
            transportLayer.nodes[environment.node.pid] = environment
            nodes.add(new Thread(environment, "node-${environment.node.pid}"))
        }

        nodes.each { it.start() }
        Thread.sleep(100)
        leaderEnvironment.delegateMsg(prepareProposal())
        nodes.each { it.join() }
    }

    private verify() {
        if (!configuration) {
            throw new Exception()
        }
    }

    private static NodeLocalTestEnvironment createEnvironment(BasicNode node) {
        return new NodeLocalTestEnvironment(node, 5000)
    }

    private Message prepareProposal() {
        // FIXME only type and payload should be meaningful for node (rest should be created by proposer node)
      return new Message(MessageType.START_SESSION, firstSessionId, -1, 1, proposal, null)
    }
}
