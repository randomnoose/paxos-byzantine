package pl.edu.agh.iosr.paxos.byzantine.acceptance

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import pl.edu.agh.iosr.paxos.byzantine.BasicNode
import pl.edu.agh.iosr.paxos.byzantine.io.TransportLayer
import pl.edu.agh.iosr.paxos.byzantine.messages.Message

import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.BlockingQueue
import java.util.concurrent.TimeUnit

class NodeLocalTestEnvironment implements Runnable {
    private static final Logger LOGGER = LogManager.getLogger(NodeLocalTestEnvironment.class)

    BlockingQueue<Message> messageQueue = new ArrayBlockingQueue<>(100)
    BasicNode node
    TransportLayer transportLayer
    long poolingTimeoutInMilliseconds


    NodeLocalTestEnvironment(BasicNode node, long poolingTimeoutInMilliseconds) {
        this.poolingTimeoutInMilliseconds = poolingTimeoutInMilliseconds
        this.node = node
    }

    void configure() {
        if (!transportLayer) {
            throw new Exception("Transport layer not found")
        }
      node.configure(transportLayer)
    }

    void delegateMsg(Message message) {
        messageQueue.add(message)
    }

    @Override
    void run() {
        def isRunning = true
        while (isRunning) {
            def message = messageQueue.poll(poolingTimeoutInMilliseconds as long, TimeUnit.MILLISECONDS)
            if (message == null) {
                LOGGER.info("Msg pooling timed out - assuming test end")
                isRunning = false
            } else {
                node.notify message
            }
        }
    }
}
