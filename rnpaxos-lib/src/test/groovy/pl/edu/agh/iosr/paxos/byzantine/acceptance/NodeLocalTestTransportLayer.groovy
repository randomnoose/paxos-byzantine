package pl.edu.agh.iosr.paxos.byzantine.acceptance

import org.apache.logging.log4j.LogManager
import pl.edu.agh.iosr.paxos.byzantine.io.Configuration
import pl.edu.agh.iosr.paxos.byzantine.io.TransportLayer
import pl.edu.agh.iosr.paxos.byzantine.messages.Message

class NodeLocalTestTransportLayer implements TransportLayer {

    private static final LOGGER = LogManager.getLogger(NodeLocalTestTransportLayer.class)
    def nodes = new HashMap() as HashMap<Long, NodeLocalTestEnvironment>
    def configuration

    NodeLocalTestTransportLayer(Configuration configuration) {
        this.configuration = configuration
    }

    @Override
    Configuration getConfiguration() {
        return configuration
    }

    @Override
    Set<Long> getPidsOfNodes() {
        return nodes.keySet()
    }

    @Override
    void sendTo(Long pid, Message message) {
        nodes[pid].delegateMsg(message)
    }
}
