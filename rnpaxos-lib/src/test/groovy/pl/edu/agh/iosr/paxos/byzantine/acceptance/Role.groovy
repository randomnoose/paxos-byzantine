package pl.edu.agh.iosr.paxos.byzantine.acceptance

enum Role {
    ACCEPTOR,
    PROPOSER,
}