package pl.edu.agh.iosr.paxos.byzantine.acceptance.scenario

import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.config.Configurator
import pl.edu.agh.iosr.paxos.byzantine.BasicNode
import pl.edu.agh.iosr.paxos.byzantine.acceptance.BasicPaxosFlow
import pl.edu.agh.iosr.paxos.byzantine.acceptance.Role
import pl.edu.agh.iosr.paxos.byzantine.io.Configuration
import pl.edu.agh.iosr.paxos.byzantine.messages.MessageType
import pl.edu.agh.iosr.paxos.byzantine.messages.StringProposal
import spock.lang.Specification

class PaxosAcceptanceSpec extends Specification {

  def setupSpec() {
    Configurator.setRootLevel(Level.ALL)
  }

  def "scenario1NiceRun"() {
    given:
    List<BasicNode> nodes = getNodes()
    def proposal = new StringProposal("firstValue")
    def configuration = new Configuration(4, 1, 3000)
    def sessionId = UUID.randomUUID()

    when:
    BasicPaxosFlow.flow()
        .withConfiguration(configuration)
        .withNodes(nodes, [Role.ACCEPTOR, Role.PROPOSER])
        .withFirstLeader(nodes[0])
        .withProposal(proposal, sessionId)
        .run()

    then:
    nodes.each { node ->
      assert node.getDecision(sessionId) == proposal
      assert node.getBaseOfDecision(sessionId) == MessageType.NOTIFY_WEAK
    }
  }

  def "scenario4Acceptors1Lying"() {
    given:
    List<BasicNode> nodes = getNodes(1)
    def proposal = new StringProposal("firstValue")
    def configuration = new Configuration(4, 1, 3000)
    def sessionId = UUID.randomUUID()

    when:
    BasicPaxosFlow.flow()
        .withConfiguration(configuration)
        .withNodes(nodes, [Role.ACCEPTOR, Role.PROPOSER])
        .withFirstLeader(nodes[0])
        .withProposal(proposal, sessionId)
        .run()

    then:
    nodes.each { node ->
      assert node.getDecision(sessionId) == proposal
      assert node.getBaseOfDecision(sessionId) == MessageType.NOTIFY_STRONG
    }
  }

  def "scenario4AcceptorsFirstLeaderLying"() {
    given:
    List<BasicNode> nodes = getNodes(2)
    def proposal = new StringProposal("firstValue")
    def configuration = new Configuration(4, 1, 3000)
    def sessionId = UUID.randomUUID()

    when:
    BasicPaxosFlow.flow()
        .withConfiguration(configuration)
        .withNodes(nodes, [Role.ACCEPTOR, Role.PROPOSER])
        .withFirstLeader(nodes[4])
        .withProposal(proposal, sessionId)
        .run()

    then:
    nodes.each { node ->
      assert node.getDecision(sessionId) == proposal
      assert node.getBaseOfDecision(sessionId) == MessageType.NOTIFY_WEAK
      assert node.getDecisionRoundNumber() == 2
    }

  }

  private static BasicNode getNode(long pid) {
    return new BasicNode(pid)
  }

  private static BasicNode getNodeWithFaultyAcceptor(long pid) {
    return BasicNode.createWithFaultyAcceptor(pid)
  }

  private static BasicNode getNodeWithFaultyProposer(long pid) {
    return BasicNode.create()
  }

  private static List<BasicNode> getNodes() {
    return [getNode(1L), getNode(2L), getNode(3L), getNode(4L)]
  }

  private static List<BasicNode> getNodes(int faultyCount) {
    if (faultyCount == 1) {
      return [getNode(1L), getNode(2L), getNode(3L), getNodeWithFaultyAcceptor(4L)]
    } else if (faultyCount == 2) {
      return [getNode(1L), getNode(2L), getNode(3L), getNodeWithFaultyProposer(4L)]
    }
  }
}
