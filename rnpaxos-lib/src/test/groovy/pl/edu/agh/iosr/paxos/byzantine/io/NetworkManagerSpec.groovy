package pl.edu.agh.iosr.paxos.byzantine.io

import pl.edu.agh.iosr.paxos.byzantine.messages.Message
import spock.lang.Specification

import static pl.edu.agh.iosr.paxos.byzantine.io.dataprovider.NetworkManagerSpecConst.*

class NetworkManagerSpec extends Specification {

  private static final int FAULTY_NODES_LIMIT = 1
  private static final int ROUND_TIMEOUT_MILLISECONDS = 3000

  def allNodesPids = [ALPHA_NODE_PID, BRAVO_NODE_PID, CHARLIE_NODE_PID, DELTA_NODE_PID] as HashSet<Long>
  def thisNodePid = ALPHA_NODE_PID
  def otherNodesPids = [BRAVO_NODE_PID, CHARLIE_NODE_PID, DELTA_NODE_PID] as HashSet<Long>
  def configuration = Configuration.of(allNodesPids.size(), FAULTY_NODES_LIMIT, ROUND_TIMEOUT_MILLISECONDS)
  def transportLayer = Mock(TransportLayer)

  def underTest = new NetworkManager(transportLayer)

  def "should notify other nodes"() {
    given:
    def message = Mock(Message) {
      getPid() >> thisNodePid
    }

    when:
    underTest.notifyOthers(message)

    then:
    transportLayer.configuration >> configuration
    transportLayer.getPidsOfNodes() >> allNodesPids
    3 * transportLayer.sendTo({ otherNodesPids.contains(it) }, message)
  }

  def "should notify only sending node"() {
    given:
    def message = Mock(Message) {
      getPid() >> thisNodePid
    }

    when:
    underTest.notifyMe(message)

    then:
    transportLayer.configuration >> configuration
    transportLayer.getPidsOfNodes() >> allNodesPids
    1 * transportLayer.sendTo(thisNodePid, message)
  }

  def "should notify all nodes"() {
    given:
    def message = Mock(Message) {
      getPid() >> thisNodePid
    }

    when:
    underTest.notifyAll(message)

    then:
    transportLayer.configuration >> configuration
    transportLayer.getPidsOfNodes() >> allNodesPids
    4 * transportLayer.sendTo({ allNodesPids.contains(it) }, message)

  }
}
