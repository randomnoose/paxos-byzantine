package pl.edu.agh.iosr.paxos.byzantine.io.dataprovider

class NetworkManagerSpecConst {
  public static long ALPHA_NODE_PID = 1L
  public static long BRAVO_NODE_PID = 2L
  public static long CHARLIE_NODE_PID = 3L
  public static long DELTA_NODE_PID = 4L
}
