package pl.edu.agh.iosr.paxos.byzantine.node.session

import pl.edu.agh.iosr.paxos.byzantine.messages.Proposal
import spock.lang.Specification

class AcceptorRoundSpec extends Specification {

    public static final long DUMMY_ROUND_NUMBER = 0L

    def underTest = new AcceptorRound(DUMMY_ROUND_NUMBER)
    def alphaNodePid = 2L
    def betaNodePid = 3L
    def alphaProposal = Mock(Proposal)
    def betaProposal = Mock(Proposal)
    def gammaProposal = Mock(Proposal)

    def "weakDecision could be written to only one"() {
        given:
        def proposal = Mock(Proposal)
        def someOtherProposal = Mock(Proposal)

        when:
        underTest.weakDecision = proposal

        then:
        underTest.weakDecision == proposal

        when:
        underTest.weakDecision = someOtherProposal

        then:
        underTest.weakDecision == proposal
    }

    def "strongDecision could be written to only one"() {
        given:
        def proposal = Mock(Proposal)
        def someOtherProposal = Mock(Proposal)

        when:
        underTest.strongDecision = proposal

        then:
        underTest.strongDecision == proposal

        when:
        underTest.strongDecision = someOtherProposal

        then:
        underTest.strongDecision == proposal
    }

    def "decision could be written to only one"() {
        given:
        def proposal = Mock(Proposal)
        def someOtherProposal = Mock(Proposal)

        when:
        underTest.decision = proposal

        then:
        underTest.decision == proposal

        when:
        underTest.decision = someOtherProposal

        then:
        underTest.decision == proposal
    }

    def "should store weak notifications without duplicates"() {
        given:

        when:
        underTest.addWeakNotification(alphaNodePid, alphaProposal)
        underTest.addWeakNotification(betaNodePid, betaProposal)
        underTest.addWeakNotification(alphaNodePid, gammaProposal)

        then:
        underTest.weakNotifications.size() == 2
        underTest.weakNotifications[alphaNodePid] == alphaProposal
        underTest.weakNotifications[betaNodePid] == betaProposal
    }

    def "should store strong notifications without duplicates"() {
        given:

        when:
        underTest.addStrongNotification(alphaNodePid, alphaProposal)
        underTest.addStrongNotification(betaNodePid, betaProposal)
        underTest.addStrongNotification(alphaNodePid, gammaProposal)

        then:
        underTest.strongNotifications.size() == 2
        underTest.strongNotifications[alphaNodePid] == alphaProposal
        underTest.strongNotifications[betaNodePid] == betaProposal
    }

    def "should store decision notifications without duplicates"() {
        given:

        when:
        underTest.addDecisionNotification(alphaNodePid, alphaProposal)
        underTest.addDecisionNotification(betaNodePid, betaProposal)
        underTest.addDecisionNotification(alphaNodePid, gammaProposal)

        then:
        underTest.decisionNotifications.size() == 2
        underTest.decisionNotifications[alphaNodePid] == alphaProposal
        underTest.decisionNotifications[betaNodePid] == betaProposal
    }
}
