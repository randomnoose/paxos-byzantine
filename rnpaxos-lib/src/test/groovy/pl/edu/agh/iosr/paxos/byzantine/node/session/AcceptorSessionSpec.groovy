package pl.edu.agh.iosr.paxos.byzantine.node.session

import spock.lang.Specification

class AcceptorSessionSpec extends Specification {

    def dummySessionId = UUID.randomUUID()
    def underTest = new AcceptorSession(dummySessionId)

    def "should create nonexistent rounds"() {
        given:
        def roundNumber = 1L
        underTest.rounds.size() == 0

        when:
        def actual = underTest.getRound(roundNumber)

        then:
        actual
        underTest.rounds.size() == 1
    }

    def "should return existing rounds"() {
        given:
        def roundNumber = 1L
        def round = Mock(AcceptorRound)
        underTest.rounds.put(roundNumber, round)
        underTest.rounds.size() == 1

        when:
        def actual = underTest.getRound(roundNumber)

        then:
        actual == round
        underTest.rounds.size() == 1
    }
}
